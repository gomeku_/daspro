#include <iostream>
using namespace std;

int main()
{
    char pelanggan, lagi;
    int bayar, lembar;

    do{
        cout << "Apakah anda pelanggan (y/t) ? ";
        cin >> pelanggan;

        if (pelanggan == 'y'){
            cout << "Banyak fotocopy (lembar) : ";
            cin >> lembar;

            bayar = 75*lembar;
            cout << "Jumlah Fotocopy : " << lembar << endl;
            cout << "Bayar : Rp." << bayar << endl;

        }
        else {
            cout << "Banyak fotocopy (lembar) : ";
            cin >> lembar;

            if (lembar >= 201){
                bayar = 80*lembar;
                cout << "Jumlah Fotocopy : " << lembar << endl;
                cout << "Bayar : Rp." << bayar << endl;
            }
            else if (lembar >= 100){
                bayar = 100*lembar;
                cout << "Jumlah Fotocopy : " << lembar << endl;
                cout << "Bayar : Rp." << bayar << endl;
            }
            else {
                bayar = 150*lembar;
                cout << "Jumlah Fotocopy : " << lembar << endl;
                cout << "Bayar : Rp." << bayar << endl;
            }
        }
        cout << "Mau lagi (y/t) ? ";
        cin >> lagi;
    }while (lagi == 'y');


    return 0;
}