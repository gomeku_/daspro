#include <iostream>
using namespace std;

int main()
{
    int penghasilan, gaji_bersih, kategori;
    double pajak;

    cout << "\n=====KATEGORI PENGHASILAN=====\n";
    cout << "#1 Pekerja\n";
    cout << "#2 Pebisnis\n";
    cout << "==============================\n\n";
    cout << "Input kategori : ";
    cin >> kategori;

    switch (kategori) {
        case 1 :
            cout << "Masukkan penghasilan : ";
            cin >> penghasilan;
            if (penghasilan <= 2000000){
                pajak = 0.1;
            }
            else if (penghasilan <= 3000000){
                pajak = 0.15;
            }
            else {
                pajak = 0.2;
            }
            gaji_bersih = penghasilan-(penghasilan*pajak);

            cout << "------------------------------\n";
            cout << "Gaji bersih = " << gaji_bersih << endl;

            break;
        case 2 :
            cout << "Masukkan penghasilan : ";
            cin >> penghasilan;
            if (penghasilan <= 2500000){
                pajak = 0.15;
            }
            else if (penghasilan <= 3500000){
                pajak = 0.2;
            }
            else {
                pajak = 0.25;
            }
            gaji_bersih = penghasilan-(penghasilan*pajak);

            cout << "------------------------------\n";
            cout << "Gaji bersih = " << gaji_bersih << endl;
    
            break;
        default:
            cout << "Kategori salah" << endl;
            break;
    }


    return 0;
}