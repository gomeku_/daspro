#include <iostream>

using namespace std;

int main ()
{
    char tujuan, kelas;
    int harga, banyakTiket, hargaTiket;
    float diskon, hargaAkhir;

    cout << "jurusan yang dipilih (1/2/3): ";
    cin >> tujuan;

    switch (tujuan) {
        case '1' : // jakarta
            cout << "kelas yang dipilih (1/2/3): ";
            cin >> kelas;

            switch (kelas) {
                case '1' :
                    harga = 70000;
                    break;
                case '2' :
                    harga = 40000;
                    break;
                case '3' :
                    harga = 10000;
                    break;
                default :
                    harga = 0;
                    break;
            }
            break;

        case '2' : // jogja
            cout << "kelas yang dipilih (1/2/3): ";
            cin >> kelas;

            switch (kelas) {
                case '1' :
                    harga = 80000;
                    break;
                case '2' :
                    harga = 50000;
                    break;
                case '3' :
                    harga = 20000;
                    diskon = harga*0.1;
                    break;
                default :
                    harga = 0;
                    break;
            }
            break;
            
        case '3' : // surabaya
            cout << "kelas yang dipilih (1/2/3): ";
            cin >> kelas;

            switch (kelas) {
                case '1' :
                    harga = 90000;
                    diskon = harga*0.1;
                    break;
                case '2' :
                    harga = 60000;
                    break;
                case '3' :
                    harga = 30000;
                    break;
                default :
                    harga = 0;
                    break;
            }
            break;

        default :
            cout << "salah" <<endl;

    }
    
    cout << "banyak tiket : ";
    cin >> banyakTiket;

    hargaTiket = harga*banyakTiket;
    cout << "harga tiket : Rp " << hargaTiket << endl;
    diskon = diskon*banyakTiket;
    cout << "diskon : Rp " << diskon << endl;
    hargaAkhir = hargaTiket-diskon;
    cout << "harga setelah diskon : Rp " << hargaAkhir <<  endl;


    return 0;
}