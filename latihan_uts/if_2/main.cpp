#include <iostream>
using namespace std;

int main()
{
    //istilah generasi berdasarkan taun lahir
    int a;
    string nama, istilah;

    cout << "Masukkan nama anda : ";
    getline (cin,nama);
    cout << "Tahun berapa anda lahir : ";
    cin >> a;

    if((a > 1943)&&(a <= 1964)){
        istilah = "Baby boomer";
    }else if ((a > 1964)&&(a <= 1979)){
        istilah = "Generasi X";
    }else if((a > 1979)&&(a <= 1994)){
        istilah = "Generasi Y";
    }else if((a > 1994)&&(a <= 2015)){
        istilah = "Generasi Z";
    }else{
        istilah ="Tidak termasuk dalam kriteria";
    }
    
    cout << nama << ", berdasarkan tahun lahir anda tergolong " << istilah << endl;

    return 0;
}