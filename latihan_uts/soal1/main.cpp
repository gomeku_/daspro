#include <iostream>

using namespace std;

int main ()
{
    int angka, jam, menit, detik;

    angka = 4000;

    jam = angka/3600;
    menit = angka%3600/60;
    detik = angka%60;

    cout << jam << " jam " << menit << " menit " << detik << " detik" << endl;

    return 0;
}