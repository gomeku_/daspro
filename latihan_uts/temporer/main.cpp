#include <iostream>
using namespace std;

int main()
{
    int a, b, temporer;

    cout << "nilai A : ";
    cin >> a;
    cout << "nilai B : ";
    cin >> b;

    temporer = a;
    a = b;
    b = temporer;

    cout << "nilai A menjadi " << a << " dan nilai B menjadi " << b << endl;

    return 0;
}