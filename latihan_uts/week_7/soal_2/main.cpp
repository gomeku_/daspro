#include <iostream>

using namespace std;

int main()
{
    int umur, juara, hadiah, hadiah_min;

    cout << "masukkan umur : ";
    cin >> umur;

    if ((umur>15)&&(umur<19)){
        cout << "masukan juara : ";
        cin >> juara;
        if (juara == 1){
            hadiah = 5000000*5/100;
            hadiah_min = 5000000-hadiah;
            cout << "hadiahnya : " << hadiah_min << endl;
        }
        else if (juara == 2){
            hadiah = 4000000*5/100;
            hadiah_min = 4000000-hadiah;
            cout << "hadiahnya : " << hadiah_min << endl;
        }
        else if (juara == 3){
            hadiah = 3000000*5/100;
            hadiah_min = 3000000-hadiah;
            cout << "hadiahnya : " << hadiah_min << endl;
        }
        else {
            cout << "input tidak valid" << endl;
        }
    }
    else if ((umur>18)&&(umur<25)){
        cout << "masukan juara : ";
        cin >> juara;
        if (juara == 1){
            hadiah = 6000000*5/100;
            hadiah_min = 6000000-hadiah;
            cout << "hadiahnya : " << hadiah_min << endl;
        }
        else if (juara == 2){
            hadiah = 5000000*5/100;
            hadiah_min = 5000000-hadiah;
            cout << "hadiahnya : " << hadiah_min << endl;
        }
        else if (juara == 3){
            hadiah = 4000000*5/100;
            hadiah_min = 4000000-hadiah;
            cout << "hadiahnya : " << hadiah_min << endl;
        }
        else {
            cout << "input tidak valid" << endl;
        }
    }
    else if (umur>24){
        cout << "masukan juara : ";
        cin >> juara;
        if (juara == 1){
            hadiah = 7000000*5/100;
            hadiah_min = 7000000-hadiah;
            cout << "hadiahnya : " << hadiah_min << endl;
        }
        else if (juara == 2){
            hadiah = 6000000*5/100;
            hadiah_min = 6000000-hadiah;
            cout << "hadiahnya : " << hadiah_min << endl;
        }
        else if (juara == 3){
            hadiah = 5000000*5/100;
            hadiah_min = 5000000-hadiah;
            cout << "hadiahnya : " << hadiah_min << endl;
        }
        else {
            cout << "input tidak valid" << endl;
        }
    }
    else {
        cout << "input tidak valid" << endl;
    }

    return 0;
}
