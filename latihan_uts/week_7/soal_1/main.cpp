#include <iostream>

using namespace std;

int main()
{
    int umur, pendapatan, tanggungan, biaya_hidup;
    char status;

    cout << "masukkan umur : ";
    cin >> umur;

    if (umur >= 18){
        cout << "sudah bekerja (y/t) ? ";
        cin >> status;
        if (status == 'y'){
            cout << "masukkan pendapatan perbulan : ";
            cin >> pendapatan;
            cout << "jumlah tanggungan : ";
            cin >> tanggungan;

            biaya_hidup = pendapatan/tanggungan;

            if (biaya_hidup < 300000){
                cout << "------------------------------------------\n";
                cout << "anda masuk kategori penduduk miskin";
            }
            else{
                cout << "------------------------------------------\n";
                cout << "anda masuk kategori bukan penduduk miskin";
            }
        }
        else{
            cout << "------------------------------------------\n";
            cout << "anda masuk kategori penduduk miskin";
        }
    }
    else{
        cout << "apakah anda masih sekolah (y/t) ? ";
        cin >> status;
        if (status == 'y'){
            cout << "------------------------------------------\n";
            cout << "anda masuk kategori bukan penduduk miskin";
        }
        else{
            cout << "------------------------------------------\n";
                cout << "anda masuk kategori penduduk miskin";
        }
    }

    return 0;
}
