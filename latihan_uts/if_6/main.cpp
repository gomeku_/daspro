#include <iostream>
using namespace std;

int main()
{
    int tipe, pilihan;
    string harga;

    cout << "\n=====LIST HARGA MOTOR====\n";
    cout << "\n======PABRIKAN MOTOR=====\n";
    cout << "#1 Honda\n";
    cout << "#2 Yamaha\n";
    cout << "#3 Suzuki\n";
    cout << "=========================\n";
    cout << "\nMotor pabrikan : ";
    cin >> pilihan;

    switch (pilihan)
    {
        case 1 :
            cout << "\n========TIPE MOTOR=======\n";
            cout << "#1 Supra X\n";
            cout << "#2 Beat\n";
            cout << "#3 Vario\n";
            cout << "#4 Megapro\n";
            cout << "=========================\n";
            cout << "\nTipe motor : ";
            cin >> tipe;

            switch (tipe)
            {
                case 1:
                    harga = "15000000";
                    cout << "Harganya : " << harga << endl;
                    break;
                case 2 :
                    harga = "18000000";
                    cout << "Harganya : " << harga << endl;
                    break;
                case 3 :
                    harga = "24000000";
                    cout << "Harganya : " << harga << endl;
                    break;
                case 4 :
                    harga = "19000000";
                    cout << "Harganya : " << harga << endl;
                    break;
                default :
                    harga = "Input tidak valid";
                    cout << harga << endl;
                    break;
            }
            break;

        case 2 :
            cout << "\n========TIPE MOTOR=======\n";
            cout << "#1 Mio\n";
            cout << "#2 Jupiter\n";
            cout << "#3 Aerox\n";
            cout << "#4 vixion\n";
            cout << "=========================\n";
            cout << "\nTipe motor : ";
            cin >> tipe;

            switch (tipe)
            {
                case 1:
                    harga = "17000000";
                    cout << "Harganya : " << harga << endl;
                    break;
                case 2 :
                    harga = "19000000";
                    cout << "Harganya : " << harga << endl;
                    break;
                case 3 :
                    harga = "28000000";
                    cout << "Harganya : " << harga << endl;
                    break;
                case 4 :
                    harga = "29000000";
                    cout << "Harganya : " << harga << endl;
                    break;
                default :
                    harga = "Input tidak valid";
                    cout << harga << endl;
                    break;
            }
            break;

        case 3 :
            cout << "\n========TIPE MOTOR=======\n";
            cout << "#1 Smash\n";
            cout << "#2 Address\n";
            cout << "#3 Satria\n";
            cout << "#4 GSX-R150\n";
            cout << "=========================\n";
            cout << "\nTipe motor : ";
            cin >> tipe;

            switch (tipe)
            {
                case 1:
                    harga = "16000000";
                    cout << "Harganya : " << harga << endl;
                    break;
                case 2 :
                    harga = "17000000";
                    cout << "Harganya : " << harga << endl;
                    break;
                case 3 :
                    harga = "25000000";
                    cout << "Harganya : " << harga << endl;
                    break;
                case 4 :
                    harga = "30000000";
                    cout << "Harganya : " << harga << endl;
                    break;
                default :
                    harga = "Input tidak valid";
                    cout << harga << endl;
                    break;
            }
            break;
    
        default:
            cout << "Input tidak valid" << endl;
            break;
    }
    

    return 0;
}